<?php
require "../partials/template.php";

function get_title()
{
    echo "Edit Item Form";
}

function get_body_contents()
{ 

//access to database
	require "../controllers/connection.php";
//from web get the id
	// var_dump($_GET);
	$id = $_GET['id'];
//get all items in database where id = to the id from web
	$item_query = "SELECT * FROM items WHERE id = $id";
//turn the items to an associative array
	$item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));
// var_dump($item);
// die();
?>
	<h1 class="text-center py-5">EDIT ITEM FORM</h1>
	<div class="container">
		<div class="col-lg-6 offset-lg-3">
			<form action="../controllers/edit-item-process.php" method="POST" enctype="multipart/form-data">
			<div class="form-group">
				<label for="name">Item Name:</label>
				<input type="text" name="name" value="<?= $item['name']?>" class="form-control">
				<label for="price">Item Price: </label>
				<input type="number" name="price" value="<?= $item['price']?>" class="form-control">
				<div class="form-group">
					<label for="description">Description:</label>
					<textarea type="text" name="description" class="form-control"><?= $item['description']?></textarea>
				</div>
				<div class="form-group">
					<label for="image">Item Image:</label>
					<input type="file" name="image" class="form-control">
				</div>
				<div class="form-group">
					<label for="category">Item Category</label>
					<select name="category" value="<?= $item['category']?>" class="form-control">
	<?php
//access to database
    require "../controllers/connection.php";

    $category_query = "SELECT * FROM categories";
    $categories = mysqli_query($conn, $category_query);
    foreach ($categories as $indiv_category)
    {
	?>
	<option value="<?php echo $indiv_category["id"] ?>"
		<?php 
			echo $indiv_category['id'] == $item['category_id'] ? "selected" : ""
		 ?>

		><?php echo $indiv_category["name"] ?></option>

<?php
    }
?>
	</select>
				</div>
				<input type="hidden" name="id" value="<?php echo $id?>">
				<div class="text-center">
					<button type="submit" class="btn btn-info">Edit Item</button>
				</div>
			</div>
			</form>
		</div>
	</div>

<?php
}

?>
