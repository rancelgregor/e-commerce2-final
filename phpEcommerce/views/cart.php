<?php 
require "../partials/template.php";

function get_title(){
	echo "Cart";
}
function get_body_contents(){
require "../controllers/connection.php";
?>
<h1 class="text-center py-5">CART PAGE</h1>
<hr>
<div class="col-lg-10 offset-lg-1">
	<table class="table table-stripped">
		<thead>
			<tr class="text-center">
				<th>Item</th>
				<th>Price</th>
				<th>Quantity</th>
				<th>Subtotal</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php 
				$total = 0;
				if(isset($_SESSION['cart'])){
					foreach($_SESSION['cart'] as $itemId => $quantity){
						$item_query = "SELECT * FROM items WHERE id = $itemId";
						$indiv_item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));

						$subtotal = $indiv_item['price']*$quantity;
						$total += $subtotal;
			?>
			<tr>
				<td><?php echo $indiv_item['name'] ?></td>
				<td><?php echo $indiv_item['price'] ?></td>
				<td><span class="spanQ"><?php echo $quantity ?></span>
					<input type="number" name="" class="form-control d-none" value="<?php echo $quantity ?>" data-id="<?php echo $itemId?>">
				</td>
				<td><?php echo number_format($subtotal, 2) ?></td>
				<td>
					<a href="../controllers/remove-from-cart-process.php?name=<?php echo $itemId ?>" class="btn btn-danger">Remove from cart</a>
				</td>
			</tr>

			<?php 
					}
				}
			 ?>
			<tr class="bg-success">
					<td></td>
					<td></td>
					<td>Total: </td>
					<td><?php echo $total?></td>
					<td>
						<a href="../controllers/remove-all-items.php?name=<?php echo $itemId ?>"" class="btn btn-danger">Empty cart</a>
					</td>
				</tr>
		</tbody>
	</table>
</div>
<script type="text/javascript" src="../assets/scripts/update-cart.js"></script>
<?php 
}
?>

