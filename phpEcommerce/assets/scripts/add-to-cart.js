// console.log("Hello");

//steps we will take
// We need to capture all the add to cart button
// We need to attach an event listener to each of the buttons
//we need to get the data(the id) from the button
// we need to get the quantity from the input
// we need to check if the quantity >0
// if yes, send the data to the controller via... fetch

//get all the buttons
//addEventListener only works when we have a single item
let addToCartBtns = document.querySelectorAll(".addToCart");
// console.log(addToCartBtns);

addToCartBtns.forEach(indiv_btn =>{
	indiv_btn.addEventListener("click", btn =>{
		let id = btn.target.getAttribute("data-id");
		console.log(id);
		let quantity = btn.target.previousElementSibling.value;
		// console.log(quantity);

		if(quantity <= 0){
			alert("Please Enter Valid Quantity")
		}else{
			//javascript cannot access backend
			//use FETCH API
			let data = new FormData;
			data.append("id", id);
			data.append("cart",quantity);

			fetch("../../controllers/add-to-cart-process.php", {
				method: "POST",
				body: data //yung isesend, value at name
			}).then(response=>{
				//-------------------------
				return response.text();
			})
			.then(res=>{
				document.getElementById('cartCount').textContent = res;
			})
		}
	})
})